// Base Imports
import React, {useState, useEffect} from 'react';

// dependencies
// import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';




export default function ProductCard(props){
    let product = props.product;

    const [ isDisabled, setIsDisabled ] = useState(0);
    const [ seats, setSeats ] = useState(30);

    useEffect(()=> {
        if (seats === 0) {
            setIsDisabled(true);
        }
    },[seats]);


    return (
        <Card>
            <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <h6>Description</h6>
                <p>{product.description}</p>
                <h6>Price</h6>
                <p>{product.price}</p>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                <Button variant="primary" onClick={() => setSeats(seats - 1)} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
