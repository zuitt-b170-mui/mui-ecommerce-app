// Base imports
import React from 'react';
import Product from '../components/ProductCard'

// Bootstrap dependencies
import Container from 'react-bootstrap/Container';

// Data Import
import products from '../mock-data/products';

export default function Products(){
    const ProductCard = products.map((products) => {
        return (
            <Product product={products}/>
        )
    })
    return(
        <Container fluid>
            {ProductCard}
        </Container>
    )
}