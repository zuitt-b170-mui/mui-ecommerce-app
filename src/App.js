// Base Imports
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import UserContext from './userContext.js';
// App Components
import AppNavbar from './components/AppNavbar.js';

// Page Components
import Home from './pages/Home.js';
import Register from './pages/Register';
import Login from './pages/Login'
import Products from './pages/Products'

export default function App() {
// localStorage.getItem - used to retrieve a piece or the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.setItem in the Login.js
	const [ user, setUser ] = useState( { access: localStorage.getItem('access') } );

	const unsetUser = () => {
		//localStorage.clear() - to clear every information that is stored inside the localStorage
		localStorage.clear();
		setUser( { access: null } )
	}

	return (
		<UserContext.Provider value={{ user, setUser, unsetUser }}>
			 <Router>
			   <AppNavbar user={user} />
			   <Routes>
			       <Route path = "/" element={<Home />} />
			       <Route path = "/register" element = {<Register />} />
			       <Route path = "/login" element = {<Login />} />
				   <Route path = "/products" element = {<Products />} />
			   </Routes>
			</Router>
		</UserContext.Provider>
		)
}